FROM mayanedms/mayanedms:2.7.3

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir /srv/app_srcs
COPY ./src/apps/document_analyzer /srv/app_srcs/document_analyzer
RUN chown -R www-data:www-data /srv/app_srcs
RUN ln -s /srv/app_srcs/document_analyzer/document_analyzer/ /usr/local/lib/python2.7/dist-packages/mayan/apps/document_analyzer
# Retain the original entrypoint and command
ENTRYPOINT ["entrypoint.sh"]
CMD ["mayan"]
